let buttons = document.querySelectorAll(".btn");


document.addEventListener("keydown", function(event){
  let key = event.code;

  for(let i = 0; i < buttons.length; i++){
      buttons[i].style.backgroundColor = "#000000";
  }

  for(let i = 0; i < buttons.length; i++){
    if("Key" + buttons[i].textContent === key){
      buttons[i].style.backgroundColor = "blue";
    }

    if(buttons[i].textContent === "Enter" && event.key === "Enter"){
      buttons[i].style.backgroundColor = "blue";
    }
  }
})